; Author: RitZ
; Purpose: This script contains different utilities for QOL improvements
; Usage: Script works in Fullscreen or Borderless Window mode with 1080p resolution (I use 1920x1080) and Default keybinds. 
; Some snippets with clicks might not fully work with other resolutions. 
; If anyone made their own coordinates for 1440p or other resolutions do share with me so I can create a separate version. You need to use Window Spy (right click AHK in taskbar) and take "client" coordinates.
; To change in-game keybinds to something else, see Defaults section below.
; Shortcuts and their uses:
;	Press F5 to suspend the script
;	Press Alt + R to reload the script (useful for reloading the script after editing it while it's already running)
;	Press Alt + Z to close Dauntless and AHK
;	Press Shift once to toggle auto-sprint - Pre-requisite: Replace "Shift" with "Y" as Sprint keybind in-game
;	Press Alt + C to accept hunt and enter airship
;	Press Alt + F to accept end of hunt and exit without looking at details of hunt
;	Press Alt + D to quick reload hunting ground chosen in the list. Once chosen, the choice is remembered until you close or reload the script. To reload the script do Alt + R.
;	Press Alt + Shift + D to quick reload private hunting ground chosen in the list. Once chosen, the choice is remembered until you close or reload the script. To reload the script do Alt + R.
;	Press Alt + O to enter how many times to buy an item at Ozz event shop (Note: If there's nothing at that position it will buy the bottommost item.)
;	Press Alt + N to do Frostfall Gift curiosity and collect them - Pre-requisite: Set Frostfall Gift in bottom right slot
;	Press Alt + B to do Frostfall Cauldron curiosity and collect them - Pre-requisite: Set Frostfall Cauldron in bottom left slot
;	Press Alt + Shift + N to drop Frostfall Gift curiosity 10 at a time and collect about 50, then repeat for the remainder of the total given - Pre-requisite: Set Frostfall Gift in bottom right slot
;	Press Alt + Shift + B to drop Frostfall Cauldron curiosity 10 at a time and collect about 50, then repeat for the remainder of the total given - Pre-requisite: Set Frostfall Cauldron in bottom left slot
;	Press Alt + Q to Repeatedly press E (to collect about 50 gifts)
;	Press Alt + 1-6 to Do a normal emote, clockwise
;	Press Alt + P to open X amount of cores at Core breaker (E.g. Choosing open by 10 and amount 2 will open 20 cores)
;	Press Alt + H to do Help am stuck
;	Press Alt + E to quick reload escalation chosen in the list. Once chosen, the choice is remembered until you close or reload the script. To reload the script do Alt + R.
;	Press Alt + Shift + E to quick reload private escalation chosen in the list. Once chosen, the choice is remembered until you close or reload the script. To reload the script do Alt + R.
;	Press Alt + L to craft X amount of items
;	Press Alt + V to do return to Ramsgate
;	Press Alt + K to spend your seasonal currency buying a selected item from reward cache, like buying patrol keys

; Defaults
CoordMode, Mouse, Client
DefaultEmoteKeybind := True
global HasEventCurrently := False ; Change this to "False" when there is no event running, otherwise Quick hunting ground and escalation functionality will not work.
global EmoteKeybind := DefaultEmoteKeybind ? "c" : "v"
; To change this keybind replace y with unused key desired (apart from special keys like Shift, Control, Alt). Remember to bind the same key in-game to Sprint
global SprintKeybind := "y"
global HuntKeybind := "h"
global InteractKeybind := "e"
global HuntingGroundChosen := ""
global HuntingGroundNames := ["Emberthorn Cove         (1 - 2)","Boreal Outpost          (1 - 3)","Revelation Rock         (2 - 4)","Restless Sands          (3 - 5)","Iron Falls              (4 - 6)","Sunderstone             (5 - 8)","Aulric's Peak           (6 - 9)","Frostmarch              (7 - 10)","Thunderwatch            (8 - 11)","Fortune's Folly         (9 - 12)","Brightwood              (10 - 13)","Conundrum Rocks         (10 - 13)","Coldrunner Key          (11 - 14)","The Snowblind Waste     (12 - 15)","Undervald Defile        (13 - 16)","Cape Fury               (14 - 17)","Hades Reach             (15 - 18)","Razorcliff Isle         (16 - 19)","The Paradox Breaks      (16 - 19)","Twilight Sanctuary      (17 - 20)","The Blazeworks          (21 - 24)"]
global HuntingGroundTotal := NumGet(&HuntingGroundNames + 4*A_PtrSize)
global HuntingGroundPrivateHunt := False
global EscalationChosen := ""
global EscalationLevelChosen := ""
global EscalationNames := ["Shock","Blaze","Umbral","Terra","Frost","Radiant","Patrol","Heroic"]
global EscalationTotal := NumGet(&EscalationNames + 4*A_PtrSize)
global EscalationPrivateHunt := False

~F6::suspend
!R::Reload
; Press Alt + Z to close Dauntless and AHK
!Z::
    Runwait, Taskkill /IM Dauntless-Win64-Shipping.exe /F
    Runwait, Taskkill /IM AutoHotkey.exe /F
    Return

; Restrict all snippets below this line to only trigger if Dauntless window is active. Comment with ; to test outside Dauntless
#IfWinActive, Dauntless

; To close an opened window with Esc key
GuiEscape:
    Gui, Cancel
    Return

; Press Shift once to toggle auto-sprint - Pre-requisite: Replace Shift with Y as Sprint keybind in-game
Shift::
    pressed := !pressed
    If pressed
    {
        SendInput {%SprintKeybind% down}
    }
    Else
    {
        SendInput {%SprintKeybind% up}
    }
    Return

; Press Alt + C to accept hunt and enter airship
!C::
    Send %HuntKeybind%
    Sleep, 100
    Click 416, 938
    Return
    
; Press Alt + F to accept end of hunt and exit without looking at details of hunt
!F::
    Loop, 4
    {
        Click 1808, 1013
        Sleep, 200
    }
    Return

; Press Alt + D to reload hunting ground chosen in the list. Once chosen, the choice is remembered until you close or reload the script. To reload the script do Alt + R.
!D::
    ReloadHuntingGround()
    Return

; Press Alt + Shift + D to reload private hunting ground chosen in the list. Once chosen, the choice is remembered until you close or reload the script. To reload the script do Alt + R.
!+D::
    ReloadHuntingGround(True)
    Return

; Press Alt + O to enter how many times to buy an item at Ozz event shop
!O::
    BuyEventItem()
    Return

; Press Alt + N to do Frostfall Gift curiosity and collect them - Pre-requisite: Set Frostfall Gift in bottom right slot
!N::
    SetAndDoCuriosityEmote("Frostfall Gift curiosity", "Please enter the amount of times to repeat bottom right curiosity slot:", 1074, 710, 1244, 840, 1074, 710, 1244, 840)
    Return

; Press Alt + B to do Frostfall Cauldron curiosity and collect them - Pre-requisite: Set Frostfall Cauldron in bottom left slot
!B::
    SetAndDoCuriosityEmote("Frostfall Cauldron curiosity", "Please enter the amount of times to repeat bottom left curiosity slot:", 1074, 710, 1244, 840, 790, 694, 456, 988)
    Return

; Press Alt + Shift + N to drop Frostfall Gift curiosity 10 at a time and collect about 50, then repeat for the remainder of the total given - Pre-requisite: Set Frostfall Gift in bottom right slot
!+N::
    SetAndDoCuriosityEmote("Frostfall Gift curiosity (Gift Party mode)", "Please enter how many to give in total for bottom right curiosity slot (enter multiple of 10):", 1074, 710, 1244, 840, 1074, 710, 1244, 840, True)
    Return

; Press Alt + Shift + B to drop Frostfall Cauldron curiosity 10 at a time and collect about 50, then repeat for the remainder of the total given - Pre-requisite: Set Frostfall Cauldron in bottom left slot
!+B::
    SetAndDoCuriosityEmote("Frostfall Cauldron curiosity (Gift Party mode)", "Please enter how many to give in total for bottom left curiosity slot (enter multiple of 10):", 1074, 710, 1244, 840, 790, 694, 456, 988, True)
    Return

; Press Alt + Q to Repeatedly press E (to collect about 50 gifts)
!Q::
    CollectMultipleTimes()
    Return

; Press Alt + 1-6 to Do a normal emote, clockwise
!1::
    DoEmote(851, 363, 719, 155)
    Return

!2::
    DoEmote(578, 352, 422, 248)
    Return

!3::
    DoEmote(751, 640, 494, 767)
    Return

!4::
    DoEmote(775, 852, 734, 918)
    Return

!5::
    DoEmote(1270, 701, 1648, 866)
    Return

!6::
    DoEmote(1244, 399, 1431, 302)
    Return

; Press Alt + P to open X amount of cores at Core breaker
!P::
    SetTimer, ChangeButtonNames, 50
    Gui +LastFound +OwnDialogs +AlwaysOnTop
    MsgBox, 35, RitZ's Core opening script, Please choose by how much to open the selected cores:
    WinShow, Dauntless
    WinActivate, Dauntless
    Key := "Enter"
    IfMsgBox, Yes
        Key := "Enter"
    Else IfMsgBox, No
        Key := "X"
    Else
        Return

    iterations := ConfigureInputBox("Core opening", "Please enter the amount of times to open selected cores:")
    Loop, %iterations%
    {
        Send {%Key%}
        Sleep, 4300
        Send {Esc}
        Send {Esc}
        Sleep, 100
    }

    ChangeButtonNames: 
    IfWinNotExist, RitZ's Core opening script
        Return  ; Keep waiting
    SetTimer, ChangeButtonNames, Off
    WinActivate
    ControlSetText, Button1, &1
    ControlSetText, Button2, &10
    Return

; Press Alt + L to craft X amount of items
!L::
    SetTimer, ChangeButtonNamesCrafting, 50
    Gui +LastFound +OwnDialogs +AlwaysOnTop
    MsgBox, 35, RitZ's Crafting script, Please choose by how much to craft the selected item:
    WinShow, Dauntless
    WinActivate, Dauntless
    Key := "R"
    ; Craft by 1
    IfMsgBox, Yes
        Key := "E"
    ; Craft by 10
    Else IfMsgBox, No
        Key := "R"
    Else
        Return

    iterations := ConfigureInputBox("Crafting", "Please enter the amount of times to craft selected item:")
    Loop, %iterations%
    {
        Send {%Key% down}
        ; Craft by 10 need more delay
        Sleep, %Key% == "E" ? 1500 : 2000
        Send {%Key% up}
    }

    ChangeButtonNamesCrafting:
    IfWinNotExist, RitZ's Crafting script
        Return  ; Keep waiting
    SetTimer, ChangeButtonNamesCrafting, Off
    WinActivate
    ControlSetText, Button1, &1
    ControlSetText, Button2, &10
    Return

; Press Alt + H to do Help am stuck
!H::
    Send {Esc}
    Sleep, 50
    Loop, 15
    {
        Send {Down}
    }
    Send {Up}
    Send {Space}
    Sleep, 50
    Send {Space}
    Return

; Press Alt + E to reload Escalation chosen in the list. Once chosen, the choice is remembered until you close or reload the script. To reload the script do Alt + R.
!E::
    ReloadEscalation()
    Return

; Press Alt + Shift + E to reload private Escalation chosen in the list. Once chosen, the choice is remembered until you close or reload the script. To reload the script do Alt + R.
!+E::
    ReloadEscalation(True)
    Return

; Press Alt + V to do return to Ramsgate
!V::
    Send %HuntKeybind%
    Sleep, 50
    Loop, 7
    {
        Send {Down}
    }
    Click 962, 881
    Send {Esc}
    Return

; Press Alt + K to spend your seasonal currency buying a selected item from reward cache, like buying patrol keys
!K::
    BuySeasonalItem()
    Return

; Functions

CollectMultipleTimes()
{
    iterations := 150
    Loop, %iterations%
    {
        Interact()
    }
}

DoEmote(startCoorX, startCoorY, endCoorX, endCoorY)
{
    Send {%EmoteKeybind% down}
    MouseClickDrag, L, startCoorX, startCoorY, endCoorX, endCoorY
    Send {%EmoteKeybind% up}
}

DoCuriosityEmote(startCoorX1, startCoorY1, endCoorX1, endCoorY1, startCoorX2, startCoorY2, endCoorX2, endCoorY2)
{
    Send {%EmoteKeybind% down}
    MouseClickDrag, L, startCoorX1, startCoorY1, endCoorX1, endCoorY1
    MouseClickDrag, L, startCoorX2, startCoorY2, endCoorX2, endCoorY2
    Send {%EmoteKeybind% up}
    Sleep, 2000
}

Interact()
{
    Send %InteractKeybind%
    Sleep, 100
}

ConfigureInputBox(scriptType, scriptDesc)
{
    Gui +LastFound +OwnDialogs +AlwaysOnTop
    InputBox, userInput, RitZ's %scriptType% script, %scriptDesc%
    WinShow, Dauntless
    WinActivate, Dauntless
    Return userInput
}

SetAndDoCuriosityEmote(scriptType, scriptDesc, startCoorX1, startCoorX2, endCoorX1, endCoorX2, startCoorY1, startCoorY2, endCoorY1, endCoorY2, giftParty := False)
{
    iterations := ConfigureInputBox(scriptType, scriptDesc)
    if (!giftParty) 
    {
        Loop, %iterations%
        {
            DoCuriosityEmote(startCoorX1, startCoorX2, endCoorX1, endCoorX2, startCoorY1, startCoorY2, endCoorY1, endCoorY2)
        }
        Sleep, 300
        iterations := (iterations*3)
        Loop, %iterations%
        {
            Interact()
        }
        Return
    }
    
    iterations := iterations//10
    Loop, %iterations%
    {
        Loop, 10
        {
            DoCuriosityEmote(startCoorX1, startCoorX2, endCoorX1, endCoorX2, startCoorY1, startCoorY2, endCoorY1, endCoorY2)
        }
        CollectMultipleTimes()
    }
}

BuyEventItem()
{
    position := ConfigureInputBox("Ozz event item auto purchase", "Please enter the position of the event item you want to buy from the top:")
    quantity := ConfigureInputBox("Ozz event item auto purchase", "Please enter how many times to purchase the item in position " . position . " from the top:")
    Loop, %quantity%
    {
        ; Reset position
        Loop, 20
        {
            Send {Up}
            Sleep, 50
        }
        iterations := position - 1
        Loop, %iterations%
        {
            Send {Down}
            Sleep, 50
        }
        Send X
        Sleep, 200
        Send {Enter}
        Sleep, 4000
    }
}

ReloadHuntingGround(privateHunt := False)
{
    ; If private use 2nd coordinate (after the "?") otherwise use 1st one
    xCoordinate := !privateHunt ? 954 : 1566
    yCoordinate := !privateHunt ? 876 : 1019

    ; If no hunting ground choice was saved, create list of choices
    If (!HuntingGroundChosen)
    {
        GetReloadHuntingGroundGUI()
        HuntingGroundPrivateHunt := privateHunt
        Return

        OnSubmitHuntingGround:
        CommonSubmitGUIOptions()
        ; Call this function again
        ReloadHuntingGround(HuntingGroundPrivateHunt)
        Return
    }

    ; Navigate to chosen hunting ground
    Send %HuntKeybind%
    Sleep, 150
    CheckEvent()
    Send {Space}
    Sleep, 150
    Loop, %HuntingGroundTotal%
    {
        Send {Up}
        Sleep, 15
    }
    iterations := HuntingGroundChosen - 1
    Loop, %iterations%
    {
        Send {Down}
        Sleep, 15
    }
    Sleep, 170
    Loop, 2
    {
        Click, %xCoordinate%, %yCoordinate%
        Sleep, 30
    }
    ContinueWithLowWpn(privateHunt)
    Send {Esc}
    Return
}

ReloadEscalation(privateHunt := False)
{
    ; If private use 2nd coordinate (after the "?") otherwise use 1st one
    xCoordinate := !privateHunt ? 954 : 1566
    yCoordinate := !privateHunt ? 876 : 1019

    ; If no escalation choice was saved, create list of choices
    If (!EscalationChosen)
    {
        GetReloadEscalationGUI()
        EscalationPrivateHunt := privateHunt
        Return

        OnSubmitEscalation:
        CommonSubmitGUIOptions()
        ; Call this function again
        ReloadEscalation(EscalationPrivateHunt)
        Return
    }

    ; Navigate to chosen escalation
    Send %HuntKeybind%
    Sleep, 150
    CheckEvent()
    Send {Down}
    Send {Space}
    Sleep, 150
    iterations := EscalationChosen - 1
    Loop, %iterations%
    {
        Send {Down}
        Sleep, 15
    }
    Send {Space}
    Sleep, 150
    iterations := EscalationLevelChosen - 1
    Loop, %iterations%
    {
        Send {Down}
        Sleep, 15
    }
    Sleep, 150
    Click, %xCoordinate%, %yCoordinate%
    ContinueWithLowWpn(privateHunt)
    Send {Esc}
    Return
}

BuySeasonalItem()
{
    price := ConfigureInputBox("Seasonal item auto purchase", "Please enter price of seasonal item:")
    currencyToSpend := ConfigureInputBox("Seasonal item auto purchase", "Please enter amount of currency you want to spend on this item:")
    iterations := currencyToSpend//price
    Loop, %iterations%
    {
        Click 1484, 636
        Sleep, 4000
    }
}

; Helpers

ArrayToConfigString(Array)
{
    Str := ""
    For Index, Value In Array
       Str .= Value . "|"
    Str .= "|"
    Return Str
}

CommonReloadGUIOptions()
{
    Gui, Destroy
    Gui +AlwaysOnTop
    Gui, -MinimizeBox -MaximizeBox
}

CommonSubmitGUIOptions()
{
    Gui, Submit
    WinShow, Dauntless
    WinActivate, Dauntless
}

GetReloadHuntingGroundGUI()
{
    CommonReloadGUIOptions()
    Gui, Add, Text,, Select the hunting ground to quick reload and press the Submit button
    huntingGroundsStr := ArrayToConfigString(HuntingGroundNames)
    Gui, Font, s10, Consolas
    Gui, Add, ListBox, w300 r%HuntingGroundTotal% vHuntingGroundChosen AltSubmit, %huntingGroundsStr%
    Gui, Add, Button, Default gOnSubmitHuntingGround, Submit
    Gui, Show, AutoSize Center, RitZ's quick hunting ground reload script
}

GetReloadEscalationGUI()
{
    CommonReloadGUIOptions()
    Gui, Add, Text,, Select the escalation and level to quick reload and press the Submit button
    escalationsStr := ArrayToConfigString(EscalationNames)
    Gui, Font, s10, Consolas
    Gui, Add, ListBox, w300 r%EscalationTotal% vEscalationChosen AltSubmit, %escalationsStr%
    Gui, Add, Radio, AltSubmit vEscalationLevelChosen, 1 - 13
    Gui, Add, Radio, AltSubmit x+20 Checked, 10 - 50
    Gui, Add, Button, Default gOnSubmitEscalation xs, Submit
    Gui, Show, AutoSize Center, RitZ's quick escalation reload script
}

ContinueWithLowWpn(privateHunt)
{
    If (privateHunt)
    {
        Sleep, 100
        Click, 842, 698
    }
}

CheckEvent()
{
    ; When there's an event, an event menu item is placed in first position in the menu, so move down by 1
    If (HasEventCurrently)
    {
        Send {Down}
    }
}