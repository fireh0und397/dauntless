; Author: RitzZ
; Purpose: Opens launcher & clicks buttons until you enter Ramsgate. 
; Steps: Replace the launcher path below in custom variables.
; Disclaimer: Your password will be in plain text in this file which is not secure. Use at your own risk.
; How it works: Goes to specific client coordinate, check color there then clicks. Waits for hardcoded loading time in between. Custom variables can be adjusted for wait time (milliseconds and color 0x then RGB code).
; F5 suspends the script

; custom variables
pathToLauncher := "C:\Program Files (x86)\Epic Games\Launcher\Portal\Binaries\Win64\EpicGamesLauncher.exe"
launcherWindowName := "Epic Games Launcher"
gameWindowName := "Dauntless"
loadLauncher := 16500
loadPHXIntro := 17500
delayAfterMessage := 9000
delayAfterPlay := 26000

; DO NOT MODIFY ANYTHING BELOW, unless you know what you are doing

; other settings
#WinActivateForce
CoordMode, Mouse, Client

ClickImage(imagePath, xStartAt:=0, yStartAt:=0, xArea:=0, yArea:=0, variance:=100)
{
	If (xArea == 0)
	{
		xArea = A_ScreenWidth
	}
	If (yArea == 0)
	{
		yArea = A_ScreenHeight
	}
	
	ImageSearch, FoundX, FoundY, xStartAt, yStartAt, xArea, yArea, *variance %imagePath%
	If (ErrorLevel = 2)
	{
		MsgBox Could not conduct the search.
		Edit
	}
	Else If (ErrorLevel = 1)
	{
		MsgBox Image could not be found on the screen with settings: %xStartAt%.%yStartAt%.%xArea%.%yArea%
		; MouseClick, right, %xStartAt%, %yStartAt%
		;Edit
	}
	Else
	{
		MouseClick, left, %FoundX%, %FoundY%
	}
	Sleep, 500
}

; main code
;IfWinNotExist("ahk_exe Dauntless.exe") or IfWinNotExist("ahk_exe EpicGamesLauncher.exe")
If !WinExist(launcherWindowName) || !WinExist(gameWindowName)
{
	Run, %pathToLauncher%
}

WinWait, %launcherWindowName%
Sleep, %loadLauncher%
WinActivate, %launcherWindowName%

ClickImage("script_images/egs_library.bmp", 20, 140, 300, 300)
ClickImage("script_images/egs_breadcrumb_dauntless.bmp", 279, 45, 1300, 750)
Sleep, 500
ClickImage("script_images/egs_launch.bmp", 279, 45, 1300, 750)
WinWait, %gameWindowName%
Sleep, loadPHXIntro
WinActivate, %gameWindowName%
ClickImage("script_images/phx_ok.bmp", 904, 625, 1000, 750)
Sleep, 3000
;ClickImage("script_images/phx_play.bmp", 904, 625, 1000, 750)
MouseClick, left, 900, 780
Exit

!r::Reload
~F5::suspend