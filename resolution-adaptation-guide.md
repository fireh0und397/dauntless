# Guide to adapt my Dauntless QOL script for other resolutions

Pre-requisite: Use Window Spy (right click on AHK taskbar icon) to get your mouse position coordinates for your resolution.  
I'll provide a description followed by line numbers of where to find the coordinates to alter (use something like notepad++ software to see your line numbers).

## Simple ones that replace coordinates of button to be clicked:

74 (accept hunt button, replace 416, 938 only where 416 is X coordinate, 938 is Y coordinate)  
81 (end hunt button)  

### Hunting Ground:

**Public hunt button:**  
361 (first X coordinate, replace 954)  
362 (first Y coordinate, replace 876)  

**Private hunt button:**  
361 (second X coordinate, replace 1566)  
362 (second Y coordinate, replace 1019)  

### Escalation:

**Public hunt button:**  
408 (first X coordinate, replace 954)  
409 (first Y coordinate, replace 876)  

**Private hunt button:**  
408 (second X coordinate, replace 1566)  
409 (second Y coordinate, replace 1019)  

**Button for solo private hunts with lower than recommended wpn lvl:**  
504

## Slightly more complicated:

You are providing coordinates to draw a diagonal line to use emote wheel. So, you provide 2 sets of X, Y coordinates representing start and end point to draw the line. Like this ⟋  
You'll have to open game, imagine where your cursor will be to start a diagonal line to select option we are targeting and where cursor will be at end. Then replace in following:

### Emotes starting top, moving down clockwise:

148 (851, 363 -> X,Y coordinate for start point | 719, 155 -> X,Y coordiante for end point)  
152  
156  
160  
164  
168  

### Curiosity:

It's the same as emotes but now we are first choosing curiosity in the wheel, then selecting specific curiosity in its wheel options. So, coordinates for 2 diagonal lines instead of 1.  
First 4 coordinates are X, Y coordinates of start and end point for curiosity option  
Next 4 coordinates are X, Y coordinates of start and end point for specific curiosity option:  

123 (1074, 710, 1244, 840 -> curiosity option, next 1074, 710, 1244, 840 -> bottom right slot where you set that slot with gift box curiosity beforehand, ingame)  
128 (1074, 710, 1244, 840 -> curiosity option, then 790, 694, 456, 988 -> bottom left slot where you set that slot with cauldron curiosity beforehand, ingame)  
133 (repeat same coordinates as line 123)  
138 (repeat same coordinates as line 128)  