**Welcome**
	
Download button is at top right. You need to install AutoHotKey and double click on downloaded script to launch it.
But first, open each script by clicking on it online or a text editor after download to read its notes at the top for the description and how to use. 

**The only script that I use and maintain is qol-snippets.ahk, the rest is deprecated or not used.**

**Contribution**

Feel free to propose improvements and scripts. 

**For auto-login script - DEPRECATED after Epic Launcher**

The clicks I used are based on client coordinates and button color at the coordinates which are supposed to work on any resolution or screen size.

If the script waits for too long before clicking, then replace the values of these custom variables in the script:

```
delayAfterPassword := 3500
delayAfterPatcherPlay := 20000
delayAfterBetaMessage := 4000
delayAfterPlay := 20000
```
Thank you ajrc0re from Reddit for these timings.